# **PBMS**

un Pense-Bete Malin et Simple.

une bete page editable pour mettre tout et n'importe quoi.

## **Installation:**

cloner le depot git, et renseignez, le fichier config.php (un example est donne)

et c'est tout....

* si vous chercher quelque chose, CTRL-F de votre navigateur et c'est fait. :)

* Si vous voulez ecrire sous format markdown, dans le fichier config rajoutez simplement:
$md=true;


## **Utilisation:**

un icone a droite du titre pour editer si vous etes logger sinon un icone pour se connecter et c'est tout

![](https://gitlab.com/linuxnico/pbms/-/raw/master/screenshot.png)


a Smart and Simple sticky note.

an editable page to put everything and anything.

## ** Installation: **

clone the git repository, and fill in the config.php file (an example is given)

and that's all....

if you search for something, CTRL-F from your browser and you're done. :)

If you want to write in markdown format, in the config file simply add:
$ md = true;


## **Use:**

an icon to the right of the title to edit if you are logged in otherwise an icon to connect and that's it

![](https://gitlab.com/linuxnico/pbms/-/raw/master/screenshot.png)
