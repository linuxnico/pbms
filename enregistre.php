<?php
   session_start();
   if (!isset($_SESSION['valid']) or $_SESSION['valid'] != true) {
     header('Location: index.php');
     exit();
   };
   require_once(dirname(__FILE__) . "/fonctions.php");
   require_once(dirname(__FILE__) . "/config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $titre ?></title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="knacss.css">
    </head>

    <body>
        <h1><?php echo $titre ?></h1>
        <?php
        // Nom du fichier à ouvrir
        if (isset($_POST['texte'])) {
		  $texte=$_POST['texte'];
          $fichier = fopen(dirname(__FILE__)."/pensebete.txt",'w');
		  fseek($fichier, 0);
          fputs($fichier,strip_tags($texte));
          if ($debug) {
	          logg('fichier enregistre '.dirname(__FILE__)."/pensebete.txt  texte: ".$texte);
		  }
        }
     header('Location: index.php');
     exit();
        ?>
    </body>
</html>
