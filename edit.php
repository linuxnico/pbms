<?php
   session_start();
   if ($_SESSION['valid'] != true) {
     header('Location: index.php');
     exit();
   };
   require_once(dirname(__FILE__) . "/config.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $titre ?> : edition</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="knacss.css">
    </head>
    <body>
        <h1><?php echo $titre ?></h1>
        <FORM action="enregistre.php" method="post">
          <input type="submit" value="Enregistrer">
          <TEXTAREA name="texte" rows=100 cols=500><?php
            $fichier = file("pensebete.txt");
        		$total = count($fichier);
        		for($i = 0; $i < $total; $i++) {
        		    echo strip_tags($fichier[$i]);
        		}
            ?></TEXTAREA>
        </FORM>
    </body>
</html>
