<?php
   session_start();
   if (isset($_SESSION['timeout']) and $_SESSION['timeout']+3600 < time()) {
     $_SESSION['valid']=false;
   };
   require_once(dirname(__FILE__) . "/config.php");
   if ($md) {
     require_once(dirname(__FILE__) . "/parsedown-1.7.4/Parsedown.php");
     $Parsedown = new Parsedown();
     $Parsedown->setSafeMode(true);
   }
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $titre ?></title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="knacss.css">
    </head>
    <body>
        <h1><?php echo $titre ?>
          <?php
            if (isset($_SESSION['valid']) and $_SESSION['valid'])
              {echo '<a href="edit.php"><img src="edit.svg" width=16 heigth=16></a>';}
		        else
              {echo '<a href="login.php"><img src="connect.svg" width=16 heigth=16></a></h1>';}
          ?></h1>
        <?php
		if (!file_exists(DIRNAME(__FILE__).'/pensebete.txt')) {fputs(fopen(DIRNAME(__FILE__).'/pensebete.txt','w'),'pensez a acheter du pain et de la biere');}
        // Nom du fichier à ouvrir
		$fichier = file("pensebete.txt");
		// Nombre total de ligne du fichier
		$total = count($fichier);
		for($i = 0; $i < $total; $i++) {
		  // On affiche ligne par ligne le contenu du fichier
		  // avec la fonction nl2br pour ajouter les sauts de lignes
    if ($md) {
     $ligne = $Parsedown->line($fichier[$i]);
   } else {
     $ligne = $fichier[$i];
   }
		  echo nl2br($ligne);
		}
        ?>
    </body>
</html>
